<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/v1Proccess')->group(function ()
{
	Route::get('/get-user', 'UserTestController@getDataUser')->name('getuser');
	Route::post('/create-user', 'UserTestController@createDataUser')->name('createuser');
	Route::get('/view-user/{id}', 'UserTestController@viewDataUser')->name('viewuser');
	Route::post('/delete-user/{id}', 'UserTestController@deleteDataUser')->name('deleteuser');
});

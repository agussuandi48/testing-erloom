<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use DB;

date_default_timezone_set('Asia/Jakarta');
class UserTestController extends Controller
{
    public function getDataUser()
    {
        $user = DB::table('user_tests')->get();

    	return response()->json($user);
    }

    public function createDataUser(Request $request)
    {
        $inputName = $request->name;
        $inputFabonacci = $request->fibonacci;

        if ($inputFabonacci == 0)
        {
            $data = array(0);            
        }
        else
        {
            for ($counter = 0; $counter < $inputFabonacci; $counter++)
            {   
                $data[] = $this->Fibonacci($counter);
            }
        }

        if (count($data) > 1)
        {
            $dataFibo = array_slice($data,-2);
            $resultFibo = ($dataFibo[0] + $dataFibo[1]);
        }
        else
        {
            $resultFibo = 0;
        }

        if ($request->idattr)
        {
            $insert = DB::table('user_tests')->where('id', $request->idattr)->update([
                'name' => $inputName, 
                'fibonacci_n' => $inputFabonacci,
                'fibonacci_result' => $resultFibo,
                'updated_at' => date('d-m-Y H:i:s')
            ]);

            if ($insert)
            {
                $result = array(
                    'code' => '00',
                    'message' => 'Selamat, Fabonacci berhasil diubah!',
                    'type' => 'success'
                );
            }
            else
            {
                $result = array(
                    'code' => '01',
                    'message' => 'Gagal, Fabonacci gagal diubah!',
                    'type' => 'warning'
                );
            }
        }
        else
        {
            $insert = DB::table('user_tests')->insert(
                [
                    'name' => $inputName, 
                    'fibonacci_n' => $inputFabonacci,
                    'fibonacci_result' => $resultFibo,
                    'created_at' => date('d-m-Y H:i:s'),
                    'updated_at' => date('d-m-Y H:i:s')
                ]
            );

            if ($insert)
            {
                $result = array(
                    'code' => '00',
                    'message' => 'Selamat, Fabonacci berhasil ditambah!',
                    'type' => 'success'
                );
            }
            else
            {
                $result = array(
                    'code' => '01',
                    'message' => 'Gagal, Fabonacci gagal ditambah!',
                    'type' => 'warning'
                );
            }
        }        

        return response()->json($result);
    }

    public function Fibonacci($number)
    {
        if ($number == 0)
        {
            return 0;
        } 
        else if ($number == 1)
        {
            return 1;     
        }
        else
        {
            return ($this->Fibonacci($number-1) + $this->Fibonacci($number-2));
        }
    }

    public function viewDataUser($id)
    {
        $data = DB::table('user_tests')->where('id', $id)->first();

        if ($data)
        {
            $result = array(
                'code' => '00',
                'message' => 'Selamat, Fabonacci berhasil dipilih!',
                'data' => $data
            );
        }
        else
        {
            $result = array(
                'code' => '01',
                'message' => 'Gagal, Fabonacci gagal dipilih!',
                'data' => $data
            );
        }

        return response()->json($result);
    }

    public function deleteDataUser($id)
    {
        $proccess = DB::table('user_tests')->where('id', $id)->delete();

        if ($proccess)
        {
            $result = array(
                'code' => '00',
                'message' => 'Selamat, Fabonacci berhasil dihapus!',
                'type' => 'success'
            );
        }
        else
        {
            $result = array(
                'code' => '01',
                'message' => 'Gagal, Fabonacci gagal dihapus!',
                'type' => 'warning'
            );
        }

        return response()->json($result);
    }
}

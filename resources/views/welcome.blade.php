<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12 py-3">
                    <form id="addItem">
                        @csrf
                        <div class="form-group">
                            <label>Name : </label>
                            <input type="text" id="name" name="name"/>
                            <input type="text" hidden id="idattr" name="idattr">
                        </div>
                        <div class="form-group">
                            <label>Fibonacci (n) : </label> 
                            <input type="text" id="fibonacci" name="fibonacci"/>
                        </div>
                        <button class="btn btn-primary create" type="submit">Create</button>
                        <button class="btn btn-primary update" hidden type="submit">Update</button>
                        <button class="btn btn-warning cancel" onclick="validateAttributUser('', 'D')" hidden type="button">Cancel</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table id="tableuser" class="table py-3">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Fibonacci (n)</th>
                                <th scope="col">Fibonacci (Result)</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- LOGIC START -->                            
                            <!-- LOGIC END -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>

{{-- GANTI JADI JQUERY 3 --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
{{-- JQUERY VALIDATION --}}
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        getDataUser()
    });
    
    $('form[id="addItem"]').validate({
        rules: {
            name: 'required',
            fibonacci: 'required'
        },
        messages: {              
            name: 'This field is required',
            fibonacci: 'This field is required'
        },
        submitHandler: submitUser
    });

    var nameuser = [];
    function getDataUser()
    {
        $('#tableuser tbody tr td').detach();
        $.ajax({
            url : '{{ route('getuser') }}',
            method : 'GET',
            success : function(response)
            {
                for (var i = 0; i < response.length; i++)
                {
                    nameuser[i] = response[i].name;
                    let data = "<tr>"
                        +"<td>"+response[i].id+"</td>"
                        +"<td>"+response[i].name+"</td>"
                        +"<td>"+response[i].fibonacci_n+"</td>"
                        +"<td>"+response[i].fibonacci_result+"</td>"
                        +"<td>"+response[i].created_at+"</td>"
                        +"<td><button type='button' class='btn btn-danger' onclick=deleteUser("+response[i].id+")>Hapus</button>"
                            +"<button type='button' class='btn btn-warning' onclick=viewUser("+response[i].id+")>Update</button>"
                        +"</td>"
                    +"</tr>"

                    $('#tableuser').append(data)
                }
            },
            error : function(err)
            {
                console.log(err)
            }
        })
    }

    function submitUser()
    {
        $.ajax({
            url: '{{ route('createuser') }}',
            method: 'POST',
            data: $('#addItem').serialize(),
            success: function(response)
            {
                if (response.code == '00')
                {
                    validateAttributUser('', 'D')
                    getDataUser()
                }
            },
            error : function(err)
            {
                console.log(err)
            }
        });
    }

    function viewUser(id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url : '/v1Proccess/view-user/'+id,
            method : 'GET',
            success : function(response)
            {
                if (response.code == '00')
                {
                    validateAttributUser(response, 'V')
                }
            },
            error : function(err)
            {
                console.log(err)
            }
        });
    }

    function validateAttributUser(obj, type)
    {
        if (type == 'V')
        {
            $('#idattr').val(obj.data.id)
            $('#name').val(obj.data.name)
            $('#fibonacci').val(obj.data.fibonacci_n)
            $('.create').prop('hidden', true)
            $('.update').prop('hidden', false)
            $('.cancel').prop('hidden', false)
        }
        else if (type == 'D')
        {
            $('#idattr').val('')
            $('#name').val('')
            $('#fibonacci').val('')
            $('.create').prop('hidden', false)
            $('.update').prop('hidden', true)
            $('.cancel').prop('hidden', true)
        }
    }

    function deleteUser(id)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url : '/v1Proccess/delete-user/'+id,
            method : 'POST',
            success : function(response)
            {
                if (response.code == '00')
                {
                    getDataUser()
                }
            },
            error : function(err)
            {
                console.log(err)
            }
        });
    }
</script>
